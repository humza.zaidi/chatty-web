This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Techinologies used in this application
- React (UI development)
- Redux (for state management)
- Material UI (UI components)
- Plop (https://plopjs.com/ - To generate files with consistency)


## How run the application
1. On the root of the folder run the api server (npm run api-server)
2. cd into public folder, run yarn install
3. After all the dependencies are intalled run yarn start.
