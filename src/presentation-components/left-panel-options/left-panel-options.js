import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import CircularProgress from '@material-ui/core/CircularProgress';
import purple from '@material-ui/core/colors/purple';
import Button from '@material-ui/core/Button';

const styles = theme => ({
	progress: {
		margin: theme.spacing.unit * 2,
	},
	text: {
		color: '#fff'
	},
	selected:{
		backgroundColor: '#b2003f'
	}
});


const LeftPanelOptions = (props) => {
	const { classes, fetchingRooms, rooms, onClickRoom, onLogout,roomId } = props;
	const clickRoom = (id) => {
		onClickRoom(id);
	}
	const logout = () => {
		onLogout();
	}
	return (
		<div>
			{
				fetchingRooms ?
					<CircularProgress className={classes.progress} style={{ color: purple[500] }} thickness={7} /> :
					<List>
						<div>
							{

								rooms.map(room => {
									return <ListItem className={ roomId === room.id ? classes.selected : null } onClick={ () => clickRoom(room.id) } button key={ room.id }>
												<ListItemText classes={{ primary: classes.text }} primary={ room.name } />
											</ListItem>
								})

							}
							<Button
								color="default"
								onClick={ () => {} }
								>
								Add Room
							</Button>
						</div>
					</List>
			}
		</div>

	)
}


LeftPanelOptions.propTypes = {
	classes: PropTypes.object.isRequired,
	fetchingRooms: PropTypes.bool.isRequired,
	rooms: PropTypes.array.isRequired,
	onClickRoom: PropTypes.func.isRequired,
	roomId: PropTypes.number
};

export default withStyles(styles)(LeftPanelOptions);