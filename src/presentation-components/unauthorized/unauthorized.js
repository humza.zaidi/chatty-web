import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
	container: {
		display: 'flex',
		justifyContent: 'center',
		alignItem: 'center',
		flexDirection: 'column',
		backgroundColor: '#f50057',
		height: '100vh'
	},
	message:{
		color: '#fff'
	}
});


const Unauthorized = (props) => {
	const { classes } = props;
	return (
		<div className={ classes.container }>
			<Typography color="default" variant="display2" align="center">
				<span className={ classes.message }>Unauthorized 404!!</span>
			</Typography>
		</div>
	)
}


Unauthorized.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Unauthorized);