import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import purple from '@material-ui/core/colors/purple';
import { UserContext } from '../../context/userContext'

const bottomMargin = 175;
const styles = theme => ({
	container: {
		marginTop: 10,
		maxHeight: `calc(100vh - ${bottomMargin}px)`,
		minHeight: `calc(100vh - ${bottomMargin}px)`,
	},
	progress: {
		margin: theme.spacing.unit * 2,
	},
	item:{
		marginBottom: 10
	},
	messageWrapper:{
		display: 'flex',
		flexDirection: 'column-reverse',
		maxHeight: `calc(100vh - ${bottomMargin}px)`,
		minHeight: `calc(100vh - ${bottomMargin}px)`,
		padding: '0 30px',
		overflow: 'auto'
	},
	messageContainer:{
		marginBottom: 10
	},
	message:{
		background: '#FFF',
		borderRadius: 15,
		padding: 10,
		maxWidth: 400,
		display: 'inline-block'
	},
	ownMessageItem: {
		alignSelf: 'flex-end'
	},
	ownMessage: {
		background: '#f50057',
		color: '#fff'
	}
});


const MessagePanel = (props) => {
	const { classes, fetchingMessages, messages } = props;
	return (
			<UserContext.Consumer>
			{
				userState => (
					<div className={ classes.container }>
						{
							fetchingMessages ?
							<CircularProgress className={classes.progress} style={{ color: purple[500] }} thickness={7} />:
							<div className={ classes.messageWrapper }>
								{
									messages.slice().reverse().map(m => {
										let yourself = (userState.username === m.name);
										return (
											<div key={ m.id } className={ (yourself) ? [classes.item, classes.ownMessageItem].join(' ') : classes.item }>
												<Typography variant="title" color="default" className={ classes.messageContainer }>
													<div  className={ (yourself) ? [classes.message, classes.ownMessage].join(' ') : classes.message }>{ m.message }</div>
												</Typography>
												{
													(!yourself) ?
														<Typography gutterBottom={ true } variant="body1" color="default" noWrap>{m.name}</Typography>:
														null
												}
											</div>
										)
									})
								}
							</div>
						}
					</div>
				)
			}
			</UserContext.Consumer>
		)
}


MessagePanel.propTypes = {
	classes: PropTypes.object.isRequired,
	fetchingMessages: PropTypes.bool.isRequired,
	messages: PropTypes.array
};

export default withStyles(styles)(MessagePanel);