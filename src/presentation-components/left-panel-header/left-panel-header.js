import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ExitToApp from '@material-ui/icons/ExitToApp'
import IconButton from '@material-ui/core/IconButton';

const styles = theme => ({
	container: {
		padding: 20,
		display: 'flex'
	},
	item: {
		flexGrow: 1
	},
	text:{
		color: '#fff'
	}
});


const LeftPanelHeader = (props) => {
	const { classes, username, loginText, onLogout } = props;

	const logout = () => {
		onLogout();
	}

	return (
		<div className={ classes.container }>
			<div className={ classes.item }>
				<Typography variant="title" color="default" noWrap>
					<span className={ classes.text }>{ username }</span>
				</Typography>
				<Typography variant="caption" color="default" noWrap>
					<span className={ classes.text }>{ loginText }</span>
				</Typography>
			</div>
			<IconButton  onClick={ () => logout() } className={classes.button} aria-label="Delete">
				<ExitToApp />
			</IconButton>
		</div>
	)
}


LeftPanelHeader.propTypes = {
	classes: PropTypes.object.isRequired,
	username: PropTypes.string.isRequired,
	loginText: PropTypes.string.isRequired,
	onLogout: PropTypes.func.isRequired,
};

export default withStyles(styles)(LeftPanelHeader);