import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import purple from '@material-ui/core/colors/purple';

const styles = theme => ({
	container:{
		padding: '8px 10px',
		backgroundColor: '#FFF'
	},
	progress: {
		margin: theme.spacing.unit * 2,
	},
	flexWrapper:{
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},
	self: {
		color:'#f50057'
	},
	username:{
		marginRight: 20,
		'&:last-child':{
			marginRight: 0,
		}
	}
});


const RoomHeader = (props) => {
	const { classes, fetchingRoomDetails, roomName, users } = props;
	return (
			<div className={ classes.container }>
				{ fetchingRoomDetails ?
				<CircularProgress className={classes.progress} style={{ color: purple[500] }} thickness={7} /> :
				<div className={ classes.flexWrapper }>
					<Typography variant="display1" color="default" noWrap>
						{ roomName }
					</Typography>
					<Typography variant="headline" color="default" noWrap className={ classes.usernames }>
						{
							users.map((user, i) => <span key={i}>
								{!!i && ", "}
								<span className={ user.loggedInUser ? [ classes.self, classes.username ].join(' ') : classes.username }>{ user.username }</span>
							</span>)
						}
					</Typography>
				</div>
				}
			</div>
		)
}


RoomHeader.propTypes = {
	classes: PropTypes.object.isRequired,
	roomName: PropTypes.string,
	users: PropTypes.array,
	fetchingRoomDetails: PropTypes.bool.isRequired
};

export default withStyles(styles)(RoomHeader);