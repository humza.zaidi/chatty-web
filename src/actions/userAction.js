export const LOGIN = 'user:LoginUser';
export const LOGOUT = 'user:LogoutUser';

export function login(username) {
	return (dispatch) => {
		dispatch({ type: LOGIN, payload: username });
	}
}

export function logout() {
	return (dispatch) => {
		dispatch({ type: LOGOUT });
	}
}


