import axios from 'axios'
import { baseUrl } from '../resolvedUrl';

export const FETCH_ROOMS_SUCCESS = 'rooms:GetAllRoomsSuccess';
export const FETCH_ROOMS_REJECTED = 'rooms:GetAllRoomsRejected';

export const FETCH_ROOM_BY_ID_SUCCESS = 'room:GetRoomByIdSuccess';
export const FETCH_ROOM_BY_ID_REJECTED = 'room:GetRoomByIdRejected';

export const FETCH_ROOM_MESSAGES_BY_ROOM_ID_SUCCESS = 'messages:GetMessagesByRoomIdSuccess';
export const FETCH_ROOM_MESSAGES_BY_ROOM_ID_REJECTED = 'messages:GetMessagesByRoomIdRejected';

export const POST_MESSAGE_TO_ROOM_SUCCESS = 'messages:PostMessageToRoomSuccess';
export const POST_MESSAGE_TO_ROOM_REJECTED = 'messages:PostMessageToRoomRejected';


export function getRooms() {
	return (dispatch) => {
		return axios.get(`${baseUrl}/rooms`)
				.then(response => dispatch({ type: FETCH_ROOMS_SUCCESS , payload: response.data}))
				.catch(err => dispatch({ type: FETCH_ROOMS_REJECTED, payload: err }))
	}
}

export function getRoomById(id) {
	return (dispatch) => {
		return axios.get(`${baseUrl}/rooms/${id}`)
				.then(response => dispatch({ type: FETCH_ROOM_BY_ID_SUCCESS , payload: response.data}))
				.catch(err => dispatch({ type: FETCH_ROOM_BY_ID_REJECTED, payload: err }))
	}
}

export function getMessagesByRoomId(id) {
	return (dispatch) => {
		return axios.get(`${baseUrl}/rooms/${id}/messages`)
				.then(response => dispatch({ type: FETCH_ROOM_MESSAGES_BY_ROOM_ID_SUCCESS , payload: response.data}))
				.catch(err => dispatch({ type: FETCH_ROOM_MESSAGES_BY_ROOM_ID_REJECTED, payload: err }))
	}
}

export function postMessage(id, username, message) {
	return (dispatch) => {
		return axios.post(`${baseUrl}/rooms/${id}/messages`, { name: username, message })
				.then(response => dispatch({ type: POST_MESSAGE_TO_ROOM_SUCCESS , payload: response.data}))
				.catch(err => dispatch({ type: POST_MESSAGE_TO_ROOM_REJECTED, payload: err }))
	}
}


