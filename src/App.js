import React, { Component }from "react";
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import pink from '@material-ui/core/colors/pink';
import { UserContext } from './context/userContext';
import UserLogin from './container-components/user-login/user-login';
import Chatrooms from './container-components/chatrooms/chatrooms';
import PrivateRoute from './container-components/private-route/private-route';


const theme = createMuiTheme({
	palette: {
		primary: pink,
	},
	typography: {
		fontFamily: 'Ropa Sans'
	},
});

const styles = theme => ({

});


class App extends Component {
	render(){
		const { userState } = this.props;
		return (
			<UserContext.Provider value={userState}>
				<Router>
					<MuiThemeProvider theme={theme}>
						<Route exact path="/" component={UserLogin}/>
						<PrivateRoute path="/Chatrooms" component={Chatrooms}/>
					</MuiThemeProvider>
				</Router>
			</UserContext.Provider>
		);
	}

}

const mapStateToProps = (state, props) => {
	const { userState } = state;
	return {
		userState
	}
}
const mapActionsToProp = {}

export default compose(
	withStyles(styles, { name: 'App' }),
	connect(mapStateToProps, mapActionsToProp)
)(App);

