import { combineReducers } from 'redux';

import userState from './userReducer';
import chatroomState from './chatroomReducer';

const rootReducer = combineReducers({
	userState,
	chatroomState
});

export default rootReducer;