import { FETCH_ROOMS_SUCCESS,
		FETCH_ROOMS_REJECTED,
		FETCH_ROOM_BY_ID_SUCCESS,
		FETCH_ROOM_BY_ID_REJECTED,
		FETCH_ROOM_MESSAGES_BY_ROOM_ID_SUCCESS,
		FETCH_ROOM_MESSAGES_BY_ROOM_ID_REJECTED,
		POST_MESSAGE_TO_ROOM_SUCCESS,
		POST_MESSAGE_TO_ROOM_REJECTED } from '../actions/chatroomAction';

const initialState = {
	rooms: [],
	roomDetails: {},
	messages: []
};

export default function(state = initialState, { type, payload }) {
	switch (type) {
		case FETCH_ROOMS_SUCCESS:
			return {...state, rooms: payload };
		case FETCH_ROOMS_REJECTED:
			throw Error('Unhandled exception'); // TODO handle Errors
		case FETCH_ROOM_BY_ID_SUCCESS:
			return {...state, roomDetails: payload };
		case FETCH_ROOM_BY_ID_REJECTED:
			throw Error('Unhandled exception'); // TODO handle Errors
		case FETCH_ROOM_MESSAGES_BY_ROOM_ID_SUCCESS:
			return {...state, messages: payload };
		case FETCH_ROOM_MESSAGES_BY_ROOM_ID_REJECTED:
			throw Error('Unhandled exception'); // TODO handle Errors
		case POST_MESSAGE_TO_ROOM_SUCCESS:
			return { ...state, messages: [...state.messages, payload] };
		case POST_MESSAGE_TO_ROOM_REJECTED:
			throw Error('Unhandled exception'); // TODO handle Errors
		default:
			return state;
	}
}
