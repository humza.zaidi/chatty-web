import { LOGIN, LOGOUT } from '../actions/userAction';

const initialState = {
	isAuthenticated: false,
	username: null,
	date: null
};

export default function(state = initialState , { type, payload }) {
	switch (type) {
		case LOGIN:
			return { ...state,isAuthenticated: true,  username: payload, date: new Date() };
		case LOGOUT:
			return { ...initialState };
		default:
			return state;
	}
}
