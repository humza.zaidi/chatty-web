import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const styles = theme => ({
	container: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'baseline',
		padding: '0 15px 15px 15px',
		backgroundColor: '#FFF',
		boxShadow: '0 2px 15px 0px #777',

	},
	textField: {
		marginLeft: theme.spacing.unit,
		marginRight: theme.spacing.unit,
		width: '100%',
	},
});

class MessageBox extends Component {
	state = {
		message: ''
	}

	onTextChange(event) {
		this.setState({ message: event.target.value });
	}

	onButtonClick() {
		const { onPostMessage } = this.props;
		if(this.state.message){
			onPostMessage(this.state.message);
			this.setState({ message: '' });
		}
	}
	onKeyPress(event){
		if(event.key === 'Enter') {
			this.onButtonClick();
		}
	}

  	render() {
		const { classes } = this.props;
		return (
			<div className={ classes.container }>
				<TextField
					id="with-placeholder"
					value={ this.state.message }
					label="Type a message..."
					className={classes.textField}
					onKeyPress={ this.onKeyPress.bind(this) }
					onChange={ this.onTextChange.bind(this) }
					margin="normal"
				/>
				<Button
					color="primary"
					className={classes.button}
					onClick={ this.onButtonClick.bind(this) }
					>
					Send
				</Button>
			</div>
		);
  	}
}

const mapStateToProps = (state, props) => {
	const { onPostMessage } = props;
	return {
		onPostMessage
	}
}
const mapActionsToProp = {}

export default compose(
	withStyles(styles, { name: 'MessageBox' }),
	connect(mapStateToProps, mapActionsToProp)
)(MessageBox);

