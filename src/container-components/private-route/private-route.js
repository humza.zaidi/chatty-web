import React,{ Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Route } from 'react-router-dom'
import { UserContext } from '../../context/userContext';
import Unauthorized from '../../presentation-components/unauthorized/unauthorized';
import { login } from '../../actions/userAction';
import authenticationHelper from '../../helper/authenticationHelper';

const styles = theme => ({
	container: {

	}
});

class PrivateRoute extends Component{
	componentDidMount(){
		const { login } = this.props;
		authenticationHelper(login);
	}
	render() {
		const { Component, rest } = this.props;
		return (
			<UserContext.Consumer>
			{
				user => (
					<Route
					{...rest}
					render={props =>
						user.isAuthenticated ? (
						<Component {...props} />
						) : <Unauthorized/>
					}
					/>
				)
			}
			</UserContext.Consumer>
		)
	}
}

const mapStateToProps = (state, props) => {
	const { component, ...rest } = props;

	return {
		Component: component,
		rest
	}
}
const mapActionsToProp = {
	login
}

export default compose(
	withStyles(styles, { name: 'PrivateRoute' }),
	connect(mapStateToProps, mapActionsToProp)
)(PrivateRoute);
