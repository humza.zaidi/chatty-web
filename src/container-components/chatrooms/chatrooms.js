import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import LeftNavigationPanel from '../left-navigation-panel/left-navigation-panel';
import RoomHeader from '../../presentation-components/room-header/room-header';
import MessagePanel from '../../presentation-components/message-panel/message-panel';
import MessageBox from '../../container-components/message-box/message-box';
import sessionStorageHelper from '../../helper/sessionStorageHelper';
import { getRooms, getRoomById, getMessagesByRoomId, postMessage } from '../../actions/chatroomAction';
import { logout } from '../../actions/userAction';
const styles = theme => ({
	root: {
		flexGrow: 1,
		zIndex: 1,
		overflow: 'hidden',
		position: 'relative',
		display: 'flex',
		height: '100vh'
	},
	content: {
		flexGrow: 1,
		minWidth: 0
	},
	toolbar: {
		boxShadow: '0 -2px 15px 0px #777'
	}
});

let initialState = {
	fetchingRooms: true,
	fetchingRoomDetails: true,
	fetchingMessages: true,
	users: [],
};

class Chatrooms extends Component {

	state = {...initialState};

	componentDidMount(){
		const { getRooms, getRoomById, getMessagesByRoomId } = this.props;
		getRooms()
			.then(({ payload }) => Promise.all([getRoomById(payload[0].id), getMessagesByRoomId(payload[0].id)]))
			.then(((response) => {
				let users = response[0].payload.users;
				this.setState({
					users: this.userList(users),
					fetchingRooms: false,
					fetchingRoomDetails: false,
					fetchingMessages: false
				})
			}))

	}

	userList(users) {
		const { userState } = this.props;
		const distinctList = new Set([userState.username, ...users]);
		let result = [];
		distinctList.forEach(user => {
			result.push({ username: user, loggedInUser: userState.username === user });
		})
		return result;
	}

	onClickRoom (id) {
		const { getRoomById, getMessagesByRoomId, chatroomState } = this.props;
		if(chatroomState.roomDetails.id !== id){
			this.setState({...initialState})
			Promise.all([getRoomById(id), getMessagesByRoomId(id)])
				.then(((response) => {
					let users = response[0].payload.users;
					this.setState({
						users: this.userList(users),
						fetchingRooms: false,
						fetchingRoomDetails: false,
						fetchingMessages: false
					})
				}))
		}
	}

	onLogout(){
		const { history, logout } = this.props;
		sessionStorageHelper.remove('logedInUser');
		logout();
		history.push('/');
	}

	onPostMessage(message){
		const { postMessage, chatroomState, userState } = this.props;
		postMessage(chatroomState.roomDetails.id, userState.username, message);
	}

	render() {
		const { classes, chatroomState } = this.props;
		return (
			<div className={classes.root}>
				<LeftNavigationPanel fetchingRooms={ this.state.fetchingRooms } onLogout={ this.onLogout.bind(this) } onClickRoom={ this.onClickRoom.bind(this) }/>
				<main className={classes.content}>
					<div className={classes.toolbar}>
						<RoomHeader fetchingRoomDetails={ this.state.fetchingRoomDetails } roomName={ chatroomState.roomDetails.name } users={ this.state.users }/>
					</div>
					<div>
						<MessagePanel fetchingMessages={ this.state.fetchingMessages } messages={ chatroomState.messages }/>
					</div>
					<div>
						<MessageBox onPostMessage={ this.onPostMessage.bind(this) } />
					</div>
				</main>
			</div>
		);
  	}
}

const mapStateToProps = (state, props) => {
	const { chatroomState, userState } = state;
	return {
		chatroomState,
		userState
	}
}
const mapActionsToProp = {
	getRooms,
	getRoomById,
	getMessagesByRoomId,
	postMessage,
	logout
}

export default compose(
	withStyles(styles, { name: 'Chatrooms' }),
	connect(mapStateToProps, mapActionsToProp)
)(Chatrooms);

