import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';


import { UserContext } from '../../context/userContext';
import LeftPanelHeader from '../../presentation-components/left-panel-header/left-panel-header';
import LeftPanelOptions from '../../presentation-components/left-panel-options/left-panel-options';

const drawerWidth = 240;
const styles = theme => ({
	drawerPaper: {
		backgroundColor: '#f50057',
		position: 'relative',
		width: drawerWidth
	},
	toolbar: theme.mixins.toolbar
});

class LeftNavigationPanel extends Component {

	state = {
		loginSinceText: ''
	}

	loginSinceText(now ,loggedInTime){
		let diffInMilliseconts = now - loggedInTime;
		let timeInMinutes = Math.floor(diffInMilliseconts/1000/60);
		if(timeInMinutes === 0) {
			return 'Just logged in';
		}else if (timeInMinutes === 1) {
			return 'Online for 1 minute';
		}else{
			return `Online for ${timeInMinutes} minutes`;
		}
	}

	componentDidMount(){
		const { userState } = this.props;

		this.setState({ loginSinceText:`${this.loginSinceText(new Date(), userState.date)}` });
		this.interval = setInterval(() => {
			this.setState({ loginSinceText:`${this.loginSinceText(new Date(), userState.date)}` });
		}, 1000);


	}

	componentWillUnmount(){
		clearInterval(this.interval);
	}



  	render() {
		const { classes, chatroomState, onClickRoom, onLogout,  fetchingRooms } = this.props;
		return (
			<UserContext.Consumer>
			{
				userState => (
					<Drawer
						variant="permanent"
						classes={{
						paper: classes.drawerPaper,
						}}
					>
						<div className={classes.toolbar}>
							<LeftPanelHeader username={ userState.username } onLogout={ onLogout }  loginText={ this.state.loginSinceText }/>
						</div>
						<LeftPanelOptions roomId={ chatroomState.roomDetails.id } onClickRoom={ onClickRoom } fetchingRooms={ fetchingRooms } rooms={ chatroomState.rooms }/>
					</Drawer>
				)
			}
			</UserContext.Consumer>
		);
  	}
}

const mapStateToProps = (state, props) => {
	const { userState, chatroomState } = state;
	const { onClickRoom, onLogout, fetchingRooms } = props;
	return {
		userState,
		chatroomState,
		onClickRoom,
		onLogout,
		fetchingRooms
	}
}
const mapActionsToProp = {

}

export default compose(
	withStyles(styles, { name: 'LeftNavigationPanel' }),
	connect(mapStateToProps, mapActionsToProp)
)(LeftNavigationPanel);

