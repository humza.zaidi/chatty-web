import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import authenticationHelper from '../../helper/authenticationHelper';
import sessionStorageHelper from '../../helper/sessionStorageHelper';
import { login } from '../../actions/userAction';

const styles = theme => ({
	container: {
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		height: '100vh'
	},
	item:{
		width: 500
	},
	textField: {
		marginLeft: theme.spacing.unit,
		marginRight: theme.spacing.unit,
		width: '100%',
	},
	margin: {
		margin: theme.spacing.unit,
	},
	button: {
		margin: theme.spacing.unit,
	},
});

class UserLogin extends Component {

	componentDidMount(){
		const { login,  history } = this.props;
		authenticationHelper(login, () => history.push(`/Chatrooms`));
	}

	state = {
		username: ''
	}

	onClick(event){
		const { login, history } = this.props;
		login(this.state.username);
		sessionStorageHelper.set('logedInUser', this.state.username);
		history.push(`/Chatrooms`);
	}

	onChangeText(event){
		this.setState({ username: event.target.value });
	}

	onKeyPress(event){
		if(event.key === 'Enter') {
			this.onClick();
		}
	}

  	render() {
		const { classes } = this.props;
		return (
			<div className={ classes.container }>
				<div className={ classes.item }>
					<TextField
						id="with-placeholder"
						label="Type your username"
						placeholder="e.g. John Doe"
						className={classes.textField}
						margin="normal"
						onKeyPress={ this.onKeyPress.bind(this) }
						onChange={ this.onChangeText.bind(this) }
					/>
				</div>
				<div className={ classes.item }>
					<Button
						onClick={ this.onClick.bind(this) }
						fullWidth={ true }
						variant="contained"
						color="secondary"
						disabled={ !this.state.username.length }
						className={classes.button}>
						Join!
					</Button>
				</div>
			</div>
		);
  	}
}

const mapStateToProps = (state, props) => {
	const { userState } = state.userState;
	return {
		userState
	}
}
const mapActionsToProp = {
	login
}

export default compose(
	withStyles(styles, { name: 'UserLogin' }),
	connect(mapStateToProps, mapActionsToProp)
)(UserLogin);

