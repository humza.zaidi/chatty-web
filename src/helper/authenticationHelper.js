import sessionStorageHelper from './sessionStorageHelper';
const authenticationHelper = (method, cb = () => {}) => {
	let user = sessionStorageHelper.get('logedInUser');
		if(user) {
			method(user);
			cb();
		}
}

export default authenticationHelper;